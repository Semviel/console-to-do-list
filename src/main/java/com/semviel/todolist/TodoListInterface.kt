package com.semviel.todolist

interface TodoListInterface {
    fun programOnStart()
    fun showTasks(tasks: List<Task>, filter: Filter = Filter.ALL, tasksPerList: Int, listNumber: Int)
    fun taskAdded()
    fun taskStatusChanged()
    fun taskDeleted()
    fun getCommand(): String?
//    fun changeTaskState(tasks: List<Task>, position: Int)
//    fun removeTask(tasks: List<Task>, position: Int)
}