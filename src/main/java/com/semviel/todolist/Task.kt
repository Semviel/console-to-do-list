package com.semviel.todolist

class Task(var description: String, var completed: Boolean = false)