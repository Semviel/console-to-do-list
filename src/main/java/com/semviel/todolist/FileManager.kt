package com.semviel.todolist

import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.io.FileWriter
import java.nio.file.Files
import java.nio.file.Paths

class FileManager {

    fun readFile(fileName: String): String {
        var output: String? = null
        val file = File(fileName)
        if (file.exists())
            try {
                output = String(Files.readAllBytes(Paths.get(fileName)))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        else {
            file.createNewFile()
            file.writeText("{ \"tasks\": [ ] }")
        }
        return output ?: "{ \"tasks\": [ ] }"
    }

    fun saveChanges(tasks: List<Task>, fileName: String) {
        val jsonArray = JSONArray()
        for (task in tasks) {
            val jsonTask = JSONObject()
            jsonTask.put("completed", task.completed.toString())
            jsonTask.put("description", task.description)
            jsonArray.put(jsonTask)
        }
        val jsonObject = JSONObject()
        jsonObject.put("tasks", jsonArray)
        val file = FileWriter(fileName, false)
        file.write(jsonObject.toString())
        file.close()
    }


}