package com.semviel.todolist

enum class Filter {
    ALL, COMPLETED, UNCOMPLETED
}