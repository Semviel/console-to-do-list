package com.semviel.todolist

import org.json.JSONObject
import java.util.*
import kotlin.system.exitProcess

class ConsolePresenter {
    private val view: TodoListInterface =
        ConsoleView(this)
    private val fileName = "todo-list.json"
    var filter = Filter.ALL
    var tasksPerList = 20
    var listNumber = 1
    val fileManager = FileManager()

    private val jsonString: String = fileManager.readFile(fileName)
    private val jsonObject = JSONObject(jsonString)
    private val jsonArray = jsonObject.getJSONArray("tasks")

    val tasks = mutableListOf<Task>()

    init {
        view.programOnStart()
        if (jsonArray.length() > 0)
            for (i in 0 until jsonArray.length()) {
                val obj = jsonArray.getJSONObject(i)
                val task = Task(
                    obj.getString("description"),
                    obj.getBoolean("completed")
                )
                tasks.add(task)
            }

        Runtime.getRuntime().addShutdownHook(Thread {
            fileManager.saveChanges(tasks, fileName)
        })

        view.showTasks(tasks, filter, tasksPerList, listNumber)
    }

    fun startListeningCommands() {
        while (true) {
            readLine(view.getCommand())
            view.showTasks(tasks, filter, tasksPerList, listNumber)
        }
    }


    private fun readLine(line: String?) {
        if (line == "" || line == null) {
            if ((listNumber) * tasksPerList > tasks.size) return else {
                listNumber += 1
            }
        } else {
            val sc = Scanner(line)
            if (sc.hasNext())
                when (sc.next()) {
                    "list" -> {
                        if (sc.hasNextInt()) {
                            listNumber = sc.nextInt()
                        }
                    }
                    "add" -> {
                        val sb = StringBuilder()
                        while (sc.hasNext()) {
                            sb.append(sc.next())
                            sb.append(' ')
                        }
                        tasks.add(Task(sb.toString()))
                        view.taskAdded()
                    }
                    "delete" -> {
                        if (sc.hasNextInt()) {
                            tasks.removeAt(sc.nextInt() - 1)
                            view.taskDeleted()
                        }
                    }
                    "exit" -> {
                        exitProcess(0)
                    }
                    "mark" -> {
                        if (sc.hasNextInt()) {
                            val id = sc.nextInt() - 1
                            tasks[id].completed = !tasks[id].completed
                            view.taskStatusChanged()
                        }
                    }
                    "filter" -> {
                        if (sc.hasNext()) {
                            when (sc.next()) {
                                "all" -> filter = Filter.ALL
                                "completed" -> filter = Filter.COMPLETED
                                "uncompleted" -> filter =
                                    Filter.UNCOMPLETED
                            }
                        }
                    }
                }
        }
    }

}