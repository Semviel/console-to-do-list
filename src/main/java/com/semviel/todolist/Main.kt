package com.semviel.todolist


fun main(args: Array<String>) {
    val presenter = ConsolePresenter()
    presenter.startListeningCommands()
}