package com.semviel.todolist

class ConsoleView(val presenter: ConsolePresenter) :
    TodoListInterface {
    override fun programOnStart() {
        println("To-do list\n"+
        "There are commands: \n"+
        "add do homework - add new task with desc. \"do homework\"\n"+
        "filter all/completed/uncompleted\n"+
        "mark 12 - change status of task 12\n"+
        "delete 12 - delete task 12\n"+
        "list 2 - if there are too many tasks, you can see next list by pressing Enter or choose specified list with these command\n"+
        "exit - close program and save changes to file")
    }

    override fun showTasks(tasks: List<Task>, filter: Filter, tasksPerList: Int, listNumber: Int) {
        val start = ((listNumber-1)*tasksPerList)
        if (start > tasks.size) {
            return
        }
        val end = if (tasks.size<start+tasksPerList) tasks.size else start+tasksPerList
        for (i in start until end) {
            if (filter == Filter.ALL || (tasks[i].completed && filter == Filter.COMPLETED || (!tasks[i].completed && filter == Filter.UNCOMPLETED))) {
                print("${i+1}. ")
                if (tasks[i].completed) print("[*] ")
                else print("[ ] ")
                println(tasks[i].description)
            }
        }
        if (end < tasks.size) println("...")
    }

    override fun taskAdded() {
        println("New task added")
    }

    override fun taskStatusChanged() {
        println("Status of task changed")
    }

    override fun taskDeleted() {
        println("Task deleted")
    }

    override fun getCommand(): String? = readLine()
}